public with sharing class FormBuilderExt {
    
    ID currentCaseId;
    
    public FormBuilderExt(ApexPages.StandardController controller) {
        // Get Council Serivce record (from Parms?)
            // Get the names of the fieldsets
            // Get the guidance text to display on the screen
        // Check if there is a case record 
        currentCaseId = ApexPages.currentPage().getParameters().get('id'); 
            // If there is a case record check & retrieve the case extensio record
            // If there isnt a case record check if a custoemrs detaisl supplied as a param
            
        // do some dynamic SOQL or similar to ensure that have the case and the case extension reocrd with all the fields retrieved to be included on the page 
    }
    
    public List<Schema.FieldSetMember> getCaseSummaryFields(){
        if(currentCaseId != null ){
            return Schema.SObjectType.Case.fieldSets.getMap().get('arcusplay2__Case_Std_FS').getFields();                           
        }        
        return null;
    }
    
    public List<Schema.FieldSetMember> getCaseExtensionFields(){
        if(currentCaseId != null ){
            return Schema.SObjectType.arcusplay2__Case_Extension__c.fieldSets.getMap().get('arcusplay2__Adult_Social_Care_FS').getFields();
            //return Schema.SObjectType.arcusplay2__Case_Extension__c.fieldSets.getMap().get('arcusplay2__Reg_Services_FS').getFields();                           
        }        
        return null;
    }

}