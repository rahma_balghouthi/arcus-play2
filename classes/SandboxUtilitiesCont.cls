/*
Author     :  Yi Fang  
Company    :  Arcus Global
Date       :  29-01-2016 
Purpose    :  SandboxUtilitiesCont class as a controller for SandboxUtils.apxc and connect to SandboxUtilsPage.vfp
Modified by: 
*/

public with sharing class SandboxUtilitiesCont { 
    public boolean isSandbox {get; private Set;}//To decide if the environment is sandbox
    public string selectedObject{get;set;}//For selected object
    public string nameFile{get;set;}//For file uploader
    public Blob contentFile{get;set;}
    String[] filelines = new String[]{};
    List<Team__c> teamUpload;//For specific object(s)
    List<Council_Service__c> councilServiceUpload;
    String srPath;//For static resource loading
 
    public SandboxUtilitiesCont() {
        SandboxUtils.SandboxCheck();// Here is a method to check that we are in a sandbox and if we are not then disable all operations, temporarily force to true for play2 environment
        //isSandbox = SandboxUtils.isSandbox();
        isSandbox = true;       
    }
    
    //Load File button action
    public Pagereference ReadFile(){
        nameFile = SandboxUtils.ReadFile(contentFile,nameFile,selectedObject,false);
        filelines = SandboxUtils.filelines;
        teamUpload = SandboxUtils.teamUpload;
        councilServiceUpload = SandboxUtils.councilServiceUpload;
        return null;
    }
    
    //Load static resource (Team/Council Service)
    public Pagereference ReadFileTeam(){       
        nameFile = SandboxUtils.loadStaticResource('TeamObjectCSV');
        SandboxUtils.ReadFile(contentFile,nameFile,'Team',true);
        filelines = SandboxUtils.filelines;
        teamUpload = SandboxUtils.teamUpload;
        return null;  
    }
    
    public Pagereference ReadFileCouncilService(){       
        nameFile = SandboxUtils.loadStaticResource('CouncilServiceObjectCSV');
        SandboxUtils.ReadFile(contentFile,nameFile,'Council Service',true);
        filelines = SandboxUtils.filelines;
        councilServiceUpload = SandboxUtils.councilServiceUpload;
        return null;  
    }
    
    //Import File button action
    public PageReference importFile(){
        SandboxUtils.importFile(selectedObject,teamUpload,councilServiceUpload);
        return null;
    }
    
    //Delete File button action    
    public PageReference deleteObject(){
        SandboxUtils.deleteObject(selectedObject);
        return null;
    }
    
    //Delete All File button action
    public PageReference deleteAll(){
        SandboxUtils.deleteAllTeamRecords(true);
        SandboxUtils.deleteAllCouncilServiceRecords(true);
        return null;
    } 
}