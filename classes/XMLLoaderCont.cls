public with sharing class XMLLoaderCont {

    public String staticResName {get; set;}
    public String staticResBody {get; private set;}
    private Dom.Document doc;
    public Field[] fields {get; private set;}
    public ID caseID {get; private set;}
    public Case newCase {get; private set;}
    public Map<String,String> XMLNamestoCaseFields {get; private set;}
    
    public class Field{
        public String name {get; private set;}
        public String value {get; private set;}
        public String id {get; private set;}
    }
    
    public XMLLoaderCont()
    {
        doc = getStaticResource();
        String name = 'testXML';
        if (staticResName != null) name= staticResName;
        StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = :name  LIMIT 1];
        //System.debug('1 SR = '+ sr);
        //System.debug('2 sr.Body = '+ sr.Body);
        staticResBody = sr.Body.toString();
        
        loadMap();
        
        //Dom.Document doc = new Dom.Document();
        
        XmlStreamReader xsr = new XmlStreamReader(staticResBody);
        parseFields(xsr);
        
        newCase = createCase();
        //insert newCase;
    
    }
    
    public Map<String,String> loadMap()
    {
        XMLNamestoCaseFields = new Map<String,String>();
        XMLNamestoCaseFields.put('TrxRef', 'Type');
        XMLNamestoCaseFields.put('TrxDate', 'arcusplay2__Logging_Date__c');
        XMLNamestoCaseFields.put('CustName', 'SuppliedName');
        XMLNamestoCaseFields.put('Amt', 'arcusplay2__Payment_Amount__c');
        XMLNamestoCaseFields.put('Reason', 'Reason');
        XMLNamestoCaseFields.put('AuthName', 'arcusplay2__Authorised_By__c');
        return  XMLNamestoCaseFields;  
    
    }
    
    public PageReference insertCase()
    {
        try
        {
            insert newCase;
            caseID = newCase.id;
            return null;
        }
        catch (Exception e)
        {
            throw e;
        }
        finally {
         return null;
        }
    }
        
    public Case createCase()
    {
        Case cs = new Case(Description = 'Case Created from XML \r\n');
        for (Field fl : fields){
            cs.description +=  fl.name + ': ' + fl.value + '\r\n';
            // need to try to map each field to a field on the case and set that field
            if (XMLNamestoCaseFields.containsKey(fl.name) )
            {
                // TO DO - support different field types.
                System.debug('createCase try to get the field from case = ' + XMLNamestoCaseFields.get(fl.name) );
                cs.get( XMLNamestoCaseFields.get(fl.name) );
                cs.put(XMLNamestoCaseFields.get(fl.name) , fl.value  );
            }
        }
        return cs;
        
    }

    public Field[] parseFields(XmlStreamReader reader) {
        fields = new Field[0];
        boolean isSafeToGetNextXmlElement = true;
        while(isSafeToGetNextXmlElement) {
            // Start at the beginning of the book and make sure that it is a book
            System.debug('reader.getEventType() = ' + reader.getEventType() + 'reader.getLocalName() = ' + reader.getLocalName()  );

            
            if (reader.getEventType() == XmlTag.CHARACTERS)
            {
                System.debug('reader.getText() = ' + reader.getText()  );
                //System.debug('reader.getLocalName() = ' + reader.getLocalName()  );
            }
            
            if ( reader.getLocalName() == 'fieldname' && reader.getEventType() == XmlTag.START_ELEMENT )
            {
                // This is a temp hack that presumes a structure of the fieldname node. Should replace it with a proper implementation. 
                // Should also work out how to grap field id attribure from the xml. Probably want that to do mapping to SF fields.
                Field field = new Field();
                System.debug(' --------- SPECIAL ----------- ');
                system.debug(' reader.getAttributeCount() = '+ reader.getAttributeCount() );
                reader.next();
                field.name = reader.getText(); 
                reader.next();
                reader.next();
                reader.next();
                reader.next();
                field.value = reader.getText(); 
                
                //reader.getAttributeCount();
                
                //field.id = reader.getAttributeValue(null, 'id');
                system.debug(' field.name = '+ field.name + '  -----  field.value = ' +field.value );
                fields.add(field);
            }
            
            
            // Always use hasNext() before calling next() to confirm 
            // that we have not reached the end of the stream
            if (reader.hasNext()) {
                reader.next();
            } else {
                isSafeToGetNextXmlElement = false;
                break;
            }
        }
        return fields;
    }
    
    private Dom.Document getStaticResource()
    {
        //String name = 'testXML2';
        //if (staticResName != null) name= staticResName;
        //StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'testXML2' LIMIT 1];
        //System.debug('1 SR = '+ sr);
        //System.debug('2 sr.Body = '+ sr.Body);
        //staticResBody = sr.Body.toString();
        //Dom.Document doc = new Dom.Document();
        //doc.load(staticResBody );
        //System.debug('4 doc = '+ doc);
        //return doc;
        return null;
        
    }
}