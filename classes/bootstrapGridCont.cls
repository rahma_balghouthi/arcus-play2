public class bootstrapGridCont {

    public List<Opportunity__c> oppList {get; Set;}
    public Opportunity__c selectedOpp {get; set;}
    
    public bootstrapGridCont()
    {
        oppList = [SELECT id,Name, Title__c, Benefit__c, Cost__c , Summary__c, Source_Service__c, Status__c FROM Opportunity__c LIMIT 100 ];
        if (oppList.size() > 0 ) selectedOpp = oppList[1];
    } 

}