/*
Author     :  Yi Fang  
Company    :  Arcus Global
Date       :  29-01-2016 
Purpose    :  SandboxUtils class under the control of SandboxUtilitiesCont.apxc
Modified by: 
*/

public class SandboxUtils {
    //define static variables to be used in the parent class
    //public static String nameFile;
    public static String[] filelines = new String[]{};
    public static List<Team__c> teamUpload;
    public static List<Council_Service__c> councilServiceUpload;
    
    public static Boolean deleteAllTeamRecords(Boolean hardDelete){ //  to implement hard delete functionality for team object
        try{
            List <Team__c> csList = new List<Team__c>([SELECT Id FROM Team__c]);
			System.debug('deleteAllTeamRecords - csList.size() = ' + csList.size());
			delete csList;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'The Team data has been deleted successfully.');
            ApexPages.addMessage(myMsg);
            return true;
        }
        catch (Exception e){
            throw e;
        }
        finally{
            return false;
        }
    }
    
    public static Boolean deleteAllCouncilServiceRecords(Boolean hardDelete){ // to implement hard delete functionality for council service object
        try{
            List <Council_Service__c> csList = new List<Council_Service__c>([SELECT Id FROM Council_Service__c]);
			System.debug('deleteAllCouncilServiceRecords - csList.size() = ' + csList.size());
			delete csList;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'The Council Service data has been deleted successfully.');
            ApexPages.addMessage(myMsg);
            return true;
        }
        catch (Exception e){
            throw e;
        }
        finally{
            return false;
        }
    }
    
    //delete the records in the selected object
    public static boolean deleteObject(String selectedObject){       
        if(selectedObject == 'N/A'){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Warning: Please select the object to be deleted.');
            ApexPages.addMessage(myMsg);
        }
        if(selectedObject == 'Team'){
            deleteAllTeamRecords(true);
        }
        if(selectedObject == 'Council Service'){
            deleteAllCouncilServiceRecords(true);
        }
        return null;
    }
   
    //Check if the current environment is sandbox, and return boolean
    public static Boolean isSandbox(){
        return [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
    }
    
    public static Boolean SandboxCheck(){
        if (isSandbox()){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Info: Page loaded in Sandbox Environment.');
            ApexPages.addMessage(myMsg);
        }
        else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: This page can only be used in sandboxes.');
            ApexPages.addMessage(myMsg);
        }
        return null;
    }
    
    //Load File to SF
    public static String ReadFile(Blob contentFile, String srString, String selectedObject, Boolean fromSR)
    {           
        String nameFile;
        if(contentFile <> null){
            nameFile = contentFile.toString();
            filelines = nameFile.split('\n');
        }
        if(srString <> null && fromSR){
            nameFile = srString;
            filelines = nameFile.split('\n');
        }
        else
            nameFile = contentFile.toString();
        
        //Check if the operated object has been selected, and the csv file has been attached.
        if(!fromSR && (selectedObject == 'N/A' || nameFile == null)){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Warning: Please select the operated object / attach the file in advance.');
            ApexPages.addMessage(myMsg);
        }
        if(selectedObject == 'Team'){ 
            teamUpload = new List<Team__c>();
            for (Integer i=1;i<filelines.size();i++){
                String[] inputvalues = new String[]{};
                inputvalues = filelines[i].split(',');
                
                Team__c a = new Team__c();
                //a.Id = inputvalues[0];
                //a.arcusplay2__Alfresco_Catagory__c = inputvalues[1];       
                //a.Owner = inputvalues[2];
                //a.arcusplay2__Parent_Team__c = inputvalues[3];
                //a.arcusplay2__Shared_Email__c = inputvalues[4];
                a.arcusplay2__Team_Manager__c = inputvalues[5];
                a.Name = inputvalues[6];
                
                try{
                    teamUpload.add(a);
                }
                catch (Exception e){
                throw e;
            	}
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Info: ' + selectedObject + ' CSV is loaded.');
                ApexPages.addMessage(myMsg);
            }    
            return nameFile;
        }
        if(selectedObject == 'Council Service'){
            councilServiceUpload = new List<Council_Service__c>();
            for (Integer i=1;i<filelines.size();i++){
                String[] inputvalues = new String[]{};
                inputvalues = filelines[i].split(',');
                
                Council_Service__c a = new Council_Service__c();
                //a.Id = inputvalues[0];
                a.Name = inputvalues[1];       
                //a.arcusplay2__Current_Processing_Time__c = decimal.valueOf(inputvalues[2]);
                a.arcusplay2__Delivered_By__c = inputvalues[3];
                //a.arcusplay2__Entitlement__c = inputvalues[4];
                //a.arcusplay2__Description__c = inputvalues[5];
                //a.arcusplay2__Expected_Processing_Completion_Date__c = date.valueOf(inputvalues[6]);
                //a.arcusplay2__Logging_URL__c = inputvalues[7];
                //a.arcusplay2__Related_System__c = inputvalues[8];
                //a.arcusplay2__Processing_Time_Comment__c = inputvalues[9];
                //a.arcusplay2__Service_Type__c = inputvalues[10];
                //a.arcusplay2__Target_Processing_Completion_Date__c = date.valueOf(inputvalues[11]);
                //a.arcusplay2__Target_Processing_Time__c = decimal.valueOf(inputvalues[12]);

                try{
                    councilServiceUpload.add(a);
                }
                catch (Exception e){
                throw e;
            	}
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Info: ' + selectedObject + ' CSV is loaded.');
                ApexPages.addMessage(myMsg);
            } 
            return nameFile;
        }
        else
            return nameFile;
    }
    
    //Import loaded file to SF
    public static boolean importFile(String selectedObject,List<Team__c> teamUp,List<Council_Service__c> councilServiceUp)
    {
        if(selectedObject == 'Team'){
            try{
                insert teamUp;
                ApexPages.Message insertSuccess= new ApexPages.Message(ApexPages.Severity.INFO,'The ' + selectedObject + ' data has been updated successfully.');
                ApexPages.addMessage(insertSuccess);
                return null;
            }
            catch (Exception e){
                throw e;
            }
            finally{
                return null;
            }
        }
        if(selectedObject == 'Council Service'){
            try{
                insert councilServiceUp;
                ApexPages.Message insertSuccess= new ApexPages.Message(ApexPages.Severity.INFO,'The ' + selectedObject + ' data has been updated successfully.');
                ApexPages.addMessage(insertSuccess);
                return null;
            }
            catch (Exception e){
                throw e;
            }
            finally{
                return null;
            }
        }
        else
            return null;        
    }
    
    //Load File from static resource
    public static string loadStaticResource(String DocName){
        StaticResource sr = [SELECT Id,NamespacePrefix,SystemModstamp FROM StaticResource WHERE Name = :DocName LIMIT 1];
        String prefix = sr.NamespacePrefix;
        if( String.isEmpty(prefix) ) {
            prefix = '';
        } else {
            //If has NamespacePrefix
            prefix += '__';
        }
        String srPath = '/resource/' + sr.SystemModstamp.getTime() + '/' + prefix + DocName; 
        PageReference pg = new PageReference( srPath );
        String srFile = pg.getContent().toString();
        ApexPages.Message insertSuccess= new ApexPages.Message(ApexPages.Severity.INFO,'The static resource ' + DocName + ' has been loaded successfully.');
        ApexPages.addMessage(insertSuccess);
        return srFile;
    }  
}