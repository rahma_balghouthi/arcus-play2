/*
Author     :  Yi Fang  
Company    :  Arcus Global
Date       :  29-01-2016 
Purpose    :  UserMGMT class under the control of UserMGMTCont.apxc
Modified by: 
*/

public class UserMGMT {
    public static integer showNumber;
    public static string showTestString;
    
	//load user info for the selected permission
    public static List<User> load(string PermissionSetName){
        List<User> userList = new List<User>([select id, name, email, IsActive from user WHERE id in (select AssigneeId from PermissionSetAssignment WHERE PermissionSet.Name = :PermissionSetName)]);
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Info: Users under the permission set ' + PermissionSetName + ' have been loaded successfully.');
        ApexPages.addMessage(myMsg);
        return userList;
    }
    
    //activate all selected users
    public static boolean ActiveAll(List<User> importedList){
        if(!importedList.isEmpty()){
            for(Integer i=0; i < importedList.size(); i++){
                User SelectedUser = importedList[i];
                SelectedUser.IsActive = true;
                Update SelectedUser;
            }
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Info: Users under the permission set have been activated.');
            ApexPages.addMessage(myMsg);
            return null;
        }
        else
            return null;
    }
    
    //deactivate all selected users
    public static boolean DeactiveAll(List<User> importedList){
        if(!importedList.isEmpty()){
            for(Integer i=0; i < importedList.size(); i++){
                User SelectedUser = importedList[i];
                SelectedUser.IsActive = false;
                Update SelectedUser;
            }
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Info: Users under the permission set have been deactivated.');
            ApexPages.addMessage(myMsg);
            return null;
        }
        else
            return null;
    }
    
    //fix email address if the tickbox is ticked
    public static boolean FixEmail(List<User> importedList, boolean FixEmailFlag){
        if(!importedList.isEmpty() && FixEmailFlag){
            for(Integer i=0; i < importedList.size(); i++){
                User SelectedUser = importedList[i];
                SelectedUser.email = importedList[i].email.replace('+', '');
                Update SelectedUser;
            }
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Info: Email addresses for Users under the permission set have been fixed.');
            ApexPages.addMessage(myMsg);
            return null;
        }
        else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'ERROR: Failed.');
            ApexPages.addMessage(myMsg);
            return null;
        }
    }
    
    //Retrieve available permissionSet List
    public static List<SelectOption> permissionSetListOptions(){
        List<SelectOption> PSOptions = new List<SelectOption>();
        List<PermissionSet> PermissionSetList = new List<PermissionSet>([SELECT Name FROM PermissionSet WHERE (Id IN (SELECT PermissionSetId from PermissionSetAssignment)) AND (not(Name LIKE 'X00%'))]);
        List<String> PermissionSetNameList = new List<String>();
        System.debug('<arcus> '+ PermissionSetList);
        for(PermissionSet p : PermissionSetList){
            PermissionSetNameList.add(p.Name);
        }
        for(String name : PermissionSetNameList){
            PSOptions.add(new SelectOption(name, name));
        }
        PSOptions.sort();

        return PSOptions;
    }
}