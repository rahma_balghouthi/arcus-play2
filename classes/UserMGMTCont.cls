/*
Author     :  Yi Fang  
Company    :  Arcus Global
Date       :  29-01-2016 
Purpose    :  UserMGMTCont class as a controller for UserMGMT.apxc and connect to UserManagementTool.vfp
Modified by: 
*/

public with sharing class UserMGMTCont {
 
    public String showTestString {get;set;}//For debug
    public Integer showNumber{get;private set;}//For debug
    public List<User> UserList;
    public boolean FixEmailFlag{get;set;}
    public string PermissionSetName{get;set;}
    
    Public UserMGMTCont(){   
        PermissionSetName = 'TeamPerm';
    }

    //load user info for the selected permission
    public PageReference load(){
        UserList = UserMGMT.load(PermissionSetName);
        showNumber = UserList.size();//For debug
        showTestString = UserList[0].email;//For debug
        return null;
    }
    
    //activate all selected users
    public PageReference ActiveAll(){
        UserMGMT.ActiveAll(UserList);
        return null;
    }
    
    //deactivate all selected users
    public PageReference DeactiveAll(){
        UserMGMT.DeactiveAll(UserList);
        return null;
    }
    
    //fix email address if the tickbox is ticked
    public PageReference FixEmail(){
        UserMGMT.FixEmail(UserList, FixEmailFlag);
        return null;
    }
    
    //Combine Activate and FixEmail function
    public PageReference FixAll(){
        UserMGMT.ActiveAll(UserList);
        UserMGMT.FixEmail(UserList, FixEmailFlag);
        return null;
    }
    
    //Retrieve dynamic permission set options
    public List<SelectOption> getPermissionSetPicklistOptions(){
        List<SelectOption> PSOptions = UserMGMT.permissionSetListOptions();
        return PSOptions;
    }
}