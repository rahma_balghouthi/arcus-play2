public class MyCanvasPageExt {
    
    public String generatedJson {get; private set;}
    public boolean debugMode {get; set;}
    private static final boolean LOG_THIS_CLASS = true;
    public String accountId;
    
    public MyCanvasPageExt(ApexPages.StandardController sc){        
        Case currentCase = (Case)sc.getRecord();
        // get URL paramter to turn on debug mode
        String paramReq = ApexPages.currentPage().getParameters().get('debug');
        if(LOG_THIS_CLASS) system.debug ('paramReq  = '+ paramReq );
        
        
        debugMode =  (paramReq=='true') ? true  : false;
        if(LOG_THIS_CLASS) system.debug ('debugMode  = '+ debugMode );
        
        if(LOG_THIS_CLASS) System.debug('<Arcus> sc.getRecord() = '+currentCase);
        String profileName = '';
        List<Profile> profiles = [SELECT name FROM Profile WHERE Id = :UserInfo.getProfileId() LIMIT 1 ];
        if(profiles.size() >0){
            profileName = profiles.get(0).name;
        }
        String roleName = '';
        List<UserRole> roles = [SELECT name FROM UserRole WHERE Id = :UserInfo.getUserRoleId() LIMIT 1 ];
        if(roles.size() >0){
            roleName = roles.get(0).name;
        }
        
        if(currentCase.Id != null ){
            System.debug('<Arcus> current case = '+currentCase);           
            List<Case> cases = [SELECT caseNumber, accountId, CS_Logging_Form_Url__c, arcusplay2__CS_Logging_System__c From Case WHERE id = :currentCase.Id  LIMIT 1];
            String logging_Form_URL = '';
            
            accountId = cases.get(0).accountId;
            if(String.isNotEmpty(cases.get(0).arcusplay2__CS_Logging_System__c) && cases.get(0).arcusplay2__CS_Logging_System__c.toLowerCase().equals('pinpoint')){
                List<Account> accounts = [SELECT Property_Address__c FROM Account WHERE id = :accountId LIMIT 1 ];
                if(accounts.size()>0){
                    List<Address__c> addresses = [SELECT X__c, Y__c, postcode__c FROM Address__c WHERE Id = :accounts[0].Property_Address__c LIMIT 1];                    
                    if(addresses.size()>0){
                        logging_Form_URL = 'https://maps.bristol.gov.uk/pinpoint/?x='+addresses[0].x__c+'&y='+addresses[0].y__c+'&sidebar=true';
                        if(LOG_THIS_CLASS) System.debug('<Arcus> the logging_Form_URL is ' + logging_Form_URL);
                    }
                }                
            }  else if(String.isNotEmpty(cases.get(0).arcusplay2__CS_Logging_System__c) && cases.get(0).arcusplay2__CS_Logging_System__c.toLowerCase().equals('fixmystreet')){
                List<Account> accounts = [SELECT Property_Address__c FROM Account WHERE id = :accountId LIMIT 1 ];
                if(accounts.size()>0){
                    List<Address__c> addresses = [SELECT postcode__c FROM Address__c WHERE Id = :accounts[0].Property_Address__c LIMIT 1];                    
                    if(addresses.size()>0){
                        logging_Form_URL = 'https://www.fixmystreet.com/around?pc='+addresses[0].postcode__c;
                        if(LOG_THIS_CLASS) System.debug('<Arcus> the logging_Form_URL is ' + logging_Form_URL);
                    }
                }       
            }else{                                               
                logging_Form_URL = cases.get(0).CS_Logging_Form_Url__c;
            }           
            Case cse  = [SELECT caseNumber 
                         FROM Case
                         WHERE ID = :currentCase.Id LIMIT 1];
            
            generatedJson = '{';
            generatedJson +='"debugMode":'+'"'+ debugMode+'"';
            generatedJson +=',"caseNumber":'+'"'+ cases[0].caseNumber+'"';
            generatedJson +=',"caseId":'+'"'+ currentCase.Id+'"';
            generatedJson+=',"currentUser":{';
            generatedJson+='"name":'+'"'+UserInfo.getName()+'"';
            generatedJson+=',"id":'+'"'+UserInfo.getUserId()+'"';
            generatedJson+=',"roleName":'+'"'+roleName+'"';
            generatedJson+=',"roleId":'+'"'+UserInfo.getUserRoleId()+'"';
            generatedJson+=',"profileId":'+'"'+UserInfo.getProfileId()+'"';
            generatedJson+=',"profileName":'+'"'+profileName+'"';
            generatedJson+='}';
            generatedJson +=',"customerOnCase":{';
            generatedJson+='"accountId":' +'"'+accountId+'"';
            generatedJson+='}';
            generatedJson+=',"URL":'+'"'+logging_Form_URL+'"';
            generatedJson+=',"targetSystem":'+'"'+cases.get(0).arcusplay2__CS_Logging_System__c+'"';
            generatedJson +='}';
        } 
        if(LOG_THIS_CLASS) System.debug('<Arcus> the generated Json string = '+generatedJson);
    }
    
    
}