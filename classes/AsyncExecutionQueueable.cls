public class AsyncExecutionQueueable implements Queueable{
    
    String accId;
    public AsyncExecutionQueueable(List<String> accounts){
        accId = accounts[0];
    }
    public void execute(QueueableContext context) {         
        if (String.isNotEmpty(accId)){
            Account a = [SELECT Id FROM Account WHERE Id = :accId LIMIT 1 ];
            a.Phone = '123456789';
            a.name = 'updated with job';
            try{
                update a;
            }catch(Exception e){
                System.debug('<Arcus> error in updating the contact '+e.getMessage());
            }
            update a;
        }
    }
    
    
}